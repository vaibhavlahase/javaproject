import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class MultiShapeGenerator extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    private Canvas my_canvas;
    private volatile boolean my_running;
    private my_Runner my_runner;
    private Button my_startButton;

    public void start(Stage stage) {
        my_canvas = new Canvas(640, 480);
        my_redraw(); // fill the canvas with white
        my_startButton = new Button("Start!");
        my_startButton.setOnAction(e -> my_doStartOrStop());
        HBox my_bottom = new HBox(my_startButton);
        my_bottom.setStyle("-fx-padding: 6px; -fx-border-color: black; -fx-border-width: 3px 0 0 0");
        my_bottom.setAlignment(Pos.CENTER);
        BorderPane my_root = new BorderPane(my_canvas);
        my_root.setBottom(my_bottom);
        Scene my_scene = new Scene(my_root);
        stage.setScene(my_scene);
        stage.setTitle("Click Start to Make Random Art!");
        stage.setResizable(false);
        stage.show();
    }

    private class my_Runner extends Thread {
        public void run() {
            while (my_running) {
                Platform.runLater(() -> my_redraw());
                try {
                    Thread.sleep(2000); // Wait two seconds between redraws.
                } catch (InterruptedException e) {

                }
            }
        }
    }

private void my_redraw() {
GraphicsContext my_g = my_canvas.getGraphicsContext2D();
double my_width = my_canvas.getWidth();
double my_height = my_canvas.getHeight();
if ( ! my_running ) {
my_g.setFill(Color.WHITE);
my_g.fillRect( 0, 0, my_width, my_height );
return;
}
Color my_randomGray = Color.hsb( 1, 0, Math.random() );

my_g.setFill(my_randomGray);
my_g.fillRect( 0, 0, my_width, my_height );
int my_artType = (int)(3*Math.random());
switch (my_artType) {
case 0:
my_g.setLineWidth(2);
for (int i = 0; i < 500; i++) {
int x1 = (int)(my_width * Math.random());
int y1 = (int)(my_height * Math.random());
int x2 = (int)(my_width * Math.random());
int y2 = (int)(my_height * Math.random());
Color randomHue = Color.hsb( 360*Math.random(), 1, 1);
my_g.setStroke(randomHue);
my_g.strokeLine(x1,y1,x2,y2);
}
break;

case 1:
for (int i = 0; i < 200; i++) {
int my_centerX = (int)(my_width * Math.random());
int my_centerY = (int)(my_height * Math.random());
Color my_randomHue = Color.hsb( 360*Math.random(), 1, 1);
my_g.setStroke(my_randomHue);
my_g.strokeOval(my_centerX - 50, my_centerY - 50, 100, 100);
}
break;
default:
my_g.setStroke(Color.BLACK);
my_g.setLineWidth(4);
for (int i = 0; i < 25; i++) {
int centerX = (int)(my_width * Math.random());
int centerY = (int)(my_height * Math.random());
int size = 30 + (int)(170*Math.random());
Color randomColor =Color.color( Math.random(), Math.random(),
Math.random() );
my_g.setFill(randomColor);
my_g.fillRect(centerX - size/2, centerY - size/2, size, size);
my_g.strokeRect(centerX - size/2, centerY - size/2, size, size);
}
break;
}
}

    private void my_doStartOrStop() {
        if (my_running == false) { // start a thread

            my_startButton.setText("Stop");
            my_runner = new my_Runner();
            my_running = true; 
            my_runner.start();
        } else { // stop the running thread
            my_startButton.setDisable(true);
            my_running = false;
            my_redraw();
            my_runner.interrupt();
            
            try {
                my_runner.join(1000); // Wait for thread to stop.
            } catch (InterruptedException e) {
            }
            my_runner = null;

            my_startButton.setText("Start");
            my_startButton.setDisable(false);
        }
    }
} 